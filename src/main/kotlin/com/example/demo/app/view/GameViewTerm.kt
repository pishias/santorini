package com.example.demo.app.view

import com.example.demo.app.elm.Program
import com.example.demo.app.elm.State
import com.example.demo.app.elm.presenter.IBasePresenter
import com.example.demo.app.model.Builder
import com.example.demo.app.model.GameStage
import com.example.demo.app.presenter.GamePresenter
import com.example.demo.app.model.Grid
import com.example.demo.app.model.Point
import io.reactivex.schedulers.Schedulers
import java.lang.StringBuilder
import java.util.*

class GameViewTerm: IGameView {
    private val gamePresenter: GamePresenter = GamePresenter(this, Program(Schedulers.trampoline()))

    override fun movePlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildWithPlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPresenter(): IBasePresenter {
        return gamePresenter
    }

    override fun render(state: State) {
        (state as GamePresenter.GameState)
                .apply { presenter(grid, builder1, builder2, next) }

    }

    private fun presenter(grid: Grid, player1: Builder, player2: Builder, next: List<Point>) {
        val out = StringBuilder("   ")
        for (c in 0 until grid.width) {
            out.append("     $c     ")
        }
        out.append(" (col) \n").append("  +")
        repeat(5) {
            out.append("----------+")
        }
        out.append("\n")
        for (row in 0 until grid.width) {
            out.append("$row |")
            for (col in 0 until grid.width) {
                var hasBuilder = false
                if ((player1.position.col == col)
                        and (player1.position.row == row)) {
                    out.append("  X ")
                    hasBuilder = true
                }
                if ((player2.position.col == col)
                        and (player2.position.row == row)) {
                    out.append("  Y ")
                    hasBuilder = true
                }
                if (!hasBuilder) out.append("    ")
                out.append(grid.cells[row][col].level)
                if (Point(row, col) in next) {
                    out.append(" (${next.indexOf(Point(row, col))}) |")
                } else {
                    out.append("     |")
                }

            }
            out.append("\n").append("  +")
            repeat(5) {
                out.append("----------+")
            }
            out.append("\n")
        }
        println(out.toString())
    }

    override fun run() {
        val scanner = Scanner(System.`in`)
        gamePresenter.init()

        var currentPlayer: Builder
        var choices: List<Point>
        while (gamePresenter.getState().gameStage != GameStage.END) {
            currentPlayer = gamePresenter.getCurrentPlayer()
            println(gamePresenter.getState().gameStage)
            println("Player $currentPlayer")
            println("Choose one of the following:")

            // TODO move these in the view controller
            if (gamePresenter.getState().gameStage.name.contains("MOVE")) {
                //gamePresenter.showAvailableMoves(currentPlayer)
                choices = gamePresenter.getState().next
                choices.forEachIndexed { index, point ->
                    println("$index) $point")
                }
                val moveChoice = scanner.nextInt()
                gamePresenter.move(currentPlayer, choices[moveChoice])
            } else {
                //gamePresenter.showAvailableBuilds(currentPlayer)
                choices = gamePresenter.getState().next
                choices.forEachIndexed { index, point ->
                    println("$index) $point")
                }
                val buildChoice = scanner.nextInt()
                gamePresenter.build(currentPlayer, choices[buildChoice])
            }
        }

    }

}

