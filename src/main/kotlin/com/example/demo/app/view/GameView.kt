package com.example.demo.app.view

import com.example.demo.app.elm.State
import com.example.demo.app.elm.Program
import com.example.demo.app.elm.presenter.IBasePresenter
import com.example.demo.app.model.Builder
import com.example.demo.app.model.Point
import com.example.demo.app.presenter.GamePresenter
import io.reactivex.schedulers.Schedulers
import java.awt.*
import java.awt.geom.Ellipse2D
import javax.swing.JFrame
import javax.swing.JPanel
import java.util.ArrayList



class GameView: IGameView {
    private val gamePresenter: GamePresenter = GamePresenter(this, Program(Schedulers.trampoline()))
    private var playerPawns: List<Shape> = listOf()
    private val playerSize = 5
    private val cellSize = 100.0

    init {
        gamePresenter.init()
        createUI()
        run()
    }

    override fun movePlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildWithPlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPresenter(): IBasePresenter {
        return gamePresenter
    }

    override fun render(state: State) {
        // draw player and building accordingly and reload
    }

    override fun run() {
        // TODO: Game Loop

        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        var currentPlayer: Builder
        var choices: List<Point>
//        while (gamePresenter.getState().gameStage != GameStage.END) {
//            currentPlayer = gamePresenter.getCurrentPlayer()
//            println(gamePresenter.getState().gameStage)
//            println("Player $currentPlayer")
//            println("Choose one of the following:")
//
//            // TODO move these in the view controller
//            if (gamePresenter.getState().gameStage.name.contains("MOVE")) {
//                gamePresenter.showAvailableMoves(currentPlayer)
//                choices = gamePresenter.getAvailableMoves(currentPlayer)
//                choices.forEachIndexed { index, point ->
//                    println("$index) $point")
//                }
//                val moveChoice = 0//scanner.nextInt()
//                gamePresenter.move(currentPlayer, choices[moveChoice])
//            } else {
//                gamePresenter.showAvailableBuilds(currentPlayer)
//                choices = gamePresenter.getAvailableBuilds(currentPlayer)
//                choices.forEachIndexed { index, point ->
//                    println("$index) $point")
//                }
//                val buildChoice = 0//scanner.nextInt()
//                gamePresenter.build(currentPlayer, choices[buildChoice])
//            }
//        }
    }

    private fun createUI() {
        gamePresenter.getState()

        val panel = Screen()

        val frame = JFrame()
        frame.title = "Title"
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.setSize(800, 800)
        frame.setLocationRelativeTo(null)
        frame.isVisible = true

        // drawPlayers
        val b1Pos = gamePresenter.getState().builder1.position
        val player1Pawn = Ellipse2D.Double(b1Pos.row * cellSize, b1Pos.col * cellSize,
                b1Pos.row * cellSize + playerSize, b1Pos.col * cellSize + playerSize)

        val b2Pos = gamePresenter.getState().builder2.position
        val player2Pawn = Ellipse2D.Double(b2Pos.row * cellSize, b2Pos.col * cellSize,
                b2Pos.row * cellSize + playerSize, b2Pos.col * cellSize + playerSize)

        this.playerPawns = listOf(player1Pawn, player2Pawn)


        panel.players = this.playerPawns

        frame.add(panel)
        panel.repaint()
    }

    fun drawGrid() {
        //gamePresenter.getState().grid
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

//    fun drawPlayers() {
//
//
//    }

    fun drawPossibleMoves() {
        //gamePresenter.getState().next
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

class Screen: JPanel() {

    var players = listOf<Shape>()

    fun Screen() {
        players = ArrayList(2)
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        for (player in players) {
            val g2d = g.create() as Graphics2D
            g2d.color = Color.RED
            g2d.draw(player)
            g2d.dispose()
        }
    }
}