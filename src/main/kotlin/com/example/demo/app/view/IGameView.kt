package com.example.demo.app.view

import com.example.demo.app.elm.State
import com.example.demo.app.elm.view.IBaseView
import com.example.demo.app.model.Builder
import com.example.demo.app.model.Point

interface IGameView : IBaseView {
    fun movePlayer(builder: Builder, target: Point)
    fun buildWithPlayer(builder: Builder, target: Point)
    fun render(state: State)
    fun run()
}