package com.example.demo.app.view

import com.example.demo.app.elm.State
import com.example.demo.app.elm.Program
import com.example.demo.app.elm.presenter.IBasePresenter
import com.example.demo.app.model.Builder
import com.example.demo.app.model.GameStage
import com.example.demo.app.model.Point
import com.example.demo.app.model.SimpleGod
import com.example.demo.app.presenter.GamePresenter
import io.reactivex.schedulers.Schedulers
import java.awt.*
import java.util.*
import javax.swing.JFrame
import javax.swing.JPanel
import kotlin.reflect.jvm.internal.impl.renderer.RenderingFormat.PLAIN




class GameView2: IGameView {
    private val gamePresenter: GamePresenter = GamePresenter(this, Program(Schedulers.trampoline()))
    private lateinit var frame: JFrame
    private lateinit var mainPanel: MyPanel


    init {
        createUI()
        run()
    }

    private fun createUI() {
        mainPanel = MyPanel(gamePresenter.getState())
        mainPanel.layout = null
        mainPanel.preferredSize = Dimension(600, 600)

        frame = JFrame()
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.contentPane.add(mainPanel)
        frame.pack()
        frame.isVisible = true
    }

    override fun movePlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun buildWithPlayer(builder: Builder, target: Point) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPresenter(): IBasePresenter {
        return gamePresenter
    }

    override fun render(state: State) {
        mainPanel.state = state as GamePresenter.GameState
        mainPanel.revalidate()
        mainPanel.repaint()
    }

    override fun run() {
        val scanner = Scanner(System.`in`)
        gamePresenter.init()


        var currentPlayer: Builder
        var choices: List<Point>
        while (gamePresenter.getState().gameStage != GameStage.END) {
            currentPlayer = gamePresenter.getCurrentPlayer()
            println(gamePresenter.getState().gameStage)
            println("Player $currentPlayer")
            println("Choose one of the following:")

            // TODO move these in the view controller
            if (gamePresenter.getState().gameStage.name.contains("MOVE")) {
                //gamePresenter.showAvailableMoves(currentPlayer)
                choices = gamePresenter.getState().next
                choices.forEachIndexed { index, point ->
                    println("$index) $point")
                }
                val moveChoice = scanner.nextInt()
                gamePresenter.move(currentPlayer, choices[moveChoice])
            } else {
                //gamePresenter.showAvailableBuilds(currentPlayer)
                choices = gamePresenter.getState().next
                choices.forEachIndexed { index, point ->
                    println("$index) $point")
                }
                val buildChoice = scanner.nextInt()
                gamePresenter.build(currentPlayer, choices[buildChoice])
            }
        }
    }

}

class MyPanel(var state: GamePresenter.GameState) : JPanel() {
    val padding = 12
    val playerSize = 50
    val cellSize = 100

    private val largeFont = Font("TimesRoman", Font.PLAIN, 40)

    override fun paintComponent(g: Graphics?) {
        if (g == null) return
        super.paintComponent(g)
        paintCells(g)
        paintPlayers(g)
        g.dispose()
    }

    private fun paintCells(g: Graphics) {
        state.grid.cells.forEach {
            it.forEach { cell ->
                val x = cell.position.row * cellSize
                val y = cell.position.col * cellSize
                val font = g.font
                g.font = largeFont
                g.drawRect(x, y, cellSize, cellSize)
                g.drawString("${cell.level}",x + padding/2,y+cellSize-padding /2)
                g.font = font
                if ((cell.position in state.next) and (state.gameStage != GameStage.END)) {
                    g.drawString("(${state.next.indexOf(cell.position)})",
                            x + padding,y + padding)
                    g.drawRect(x + padding,
                            y + padding,
                            cellSize - padding*2,
                            cellSize - padding*2)
                }
            }
        }
    }

    private fun paintPlayers(g: Graphics) {
        val color = g.color
        g.color = Color.GREEN
        g.fillRect(state.builder1.position.row * cellSize + padding*2,
                state.builder1.position.col * cellSize + padding*2,
                playerSize,
                playerSize)
        g.color = Color.BLUE
        g.fillRect(state.builder2.position.row * cellSize + padding*2,
                state.builder2.position.col * cellSize + padding*2,
                playerSize,
                playerSize)
        g.color = color
    }
}

