package com.example.demo.app.view

import com.example.demo.app.elm.presenter.IBasePresenter
import com.example.demo.app.presenter.GamePresenter
import javax.inject.Inject

class GameController {
    @Inject
    lateinit var gamePresenter: GamePresenter

    fun getPresenter(): IBasePresenter {
        return gamePresenter
    }
}