package com.example.demo.app.model

//TODO: builder should hold an instance of builder (easier to pass around builder than builder and then need to figure out which builder to use
//
//data class GameState(val builder1: Player,
//                     val builder2: Player,
//                     val grid: Grid,
//                     val lastPlayed: Int,
//                     val gameOver: Boolean = false)

//
//data class Move(val grid: Grid, val builder: Player, val from: Point, val target: Point): Action
//data class Build(val grid: Grid, val builder: Player, val from: Point, val target: Point): Action
//
//sealed class GeneratedAction(val grid: Grid, val builder: Player, val from: Point, val target: Point): Action {
//    class InvalidMove(grid: Grid, builder: Player, from: Point, target: Point): GeneratedAction(grid, builder, from, target)
//    class PlayMove(grid: Grid, builder: Player, from: Point, target: Point): GeneratedAction(grid, builder, from, target)
//    class PlayBuild(grid: Grid, builder: Player, from: Point, target: Point): GeneratedAction(grid, builder, from, target)
//    class LostGame(grid: Grid, builder: Player, from: Point, target: Point): GeneratedAction(grid, builder, from, target)
//    class WinGame(grid: Grid, builder: Player, from: Point, target: Point): GeneratedAction(grid, builder, from, target)
//
////    When one builder affects another
////    sealed class DefinitiveAction(offense: Int, defense: Int, point: Point, val ship: Ship): GeneratedAction(offense, defense, point) {
////        class HitMove(offense: Int, defense: Int, point: Point, ship: Ship) : DefinitiveAction(offense, defense, point, ship)
////
////    }
//}
//
//object InvalidState: Action

//fun stateValidityMiddleware(state: GameState, action: Action, dispatch: Dispatch, next: Next<GameState>): Action {
//    when(action) {
//        is Move -> {
//            if (!state.hasBoardById(action.offense) || !state.hasBoardById(action.defense)) {
//                return InvalidState
//            }
//        }
//
//        is GeneratedAction -> {
//            if (!state.hasBoardById(action.offense) || !state.hasBoardById(action.defense)) {
//                return InvalidState
//            }
//        }
//
//        is AddShip -> {
//            if (!state.hasBoardById(action.offense)) {
//                return InvalidState
//            }
//        }
//    }
//
//    return next(state, action, dispatch)
//}


//fun moveMiddleware(state: GameState, action: Action, dispatch: Dispatch, next: Next<GameState>): Action {
//    if (action is Move) {
//        val builder = action.builder.builders.find { it.position == action.from }
//                ?: return next(state, GeneratedAction.InvalidMove(action.grid, action.builder, action.from, action.target), dispatch)
//
//        val availableMoves = builder.getAvailableMoves(action.grid)
//        return when {
//            availableMoves.isEmpty() ->
//                next(state, GeneratedAction.LostGame(action.grid, action.builder, action.from, action.target), dispatch)
//            !action.builder.builders.map { it.position }.contains(action.from)
//                    or !availableMoves.contains(action.target) ->
//                next(state, GeneratedAction.InvalidMove(action.grid, action.builder, action.from, action.target), dispatch)
//            else ->
//                next(state, GeneratedAction.PlayMove(action.grid, action.builder, action.from, action.target), dispatch)
//        }
//    }
//    return next(state, action, dispatch)
//}


//fun buildMiddleware(state: GameState, action: Action, dispatch: Dispatch, next: Next<GameState>): Action {
//    if (action is Build) {
//        val builder = action.builder.builders.find { it.position == action.from }
//                ?: return next(state, GeneratedAction.InvalidMove(action.grid, action.builder, action.from, action.target), dispatch)
//
//        val availableBuilds = builder.getAvailableBuilds(action.grid)
//        return when {
//            availableBuilds.isEmpty() ->
//                next(state, GeneratedAction.LostGame(action.grid, action.builder, action.from, action.target), dispatch)
//            !action.builder.builders.map { it.position }.contains(action.from)
//                    or !availableBuilds.contains(action.target) ->
//                next(state, GeneratedAction.InvalidMove(action.grid, action.builder, action.from, action.target), dispatch)
//            else ->
//                next(state, GeneratedAction.PlayBuild(action.grid, action.builder, action.from, action.target), dispatch)
//        }
//    }
//    return next(state, action, dispatch)
//}