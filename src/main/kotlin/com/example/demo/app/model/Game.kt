package com.example.demo.app.model

import com.example.demo.app.model.Direction.*
import com.example.demo.app.presenter.GamePresenter
import javax.activity.InvalidActivityException

enum class TurnStage {
    MOVE,
    BUILD
}

enum class GameStage {
    PLAYER_1_MOVE,
    PLAYER_1_BUILD,
    PLAYER_2_MOVE,
    PLAYER_2_BUILD,
    END
}

enum class Direction {
    UP, UP_LEFT, LEFT, DOWN_LEFT,
    DOWN, DOWN_RIGHT, RIGHT, UP_RIGHT
}

data class Point(val row: Int = 0, val col: Int = 0): Comparable<Point> {
    override fun compareTo(other: Point): Int {
        return when {
            row > other.row -> 1
            row < other.row -> -1
            else -> return when {
                col > other.col -> 1
                col < other.col -> -1
                else -> 0
            }
        }
    }

    init {
        if ((row<0) or (col<0)) throw IllegalStateException()
    }
}

data class Cell(val position: Point, val level: Int = 0){
    constructor(row: Int, col: Int, level: Int = 0) : this(Point(row, col), level)

    init {
        if ((level < 0) or (level > 4)) throw IllegalStateException()
    }

    fun build(): Cell {
        return if (canActHere()) {
            Cell(position, level+1)
        } else {
            throw InvalidActivityException()
        }
    }

    fun canActHere(): Boolean {
        return level < 4
    }
}

// may need to change Grid slightly to make updating more intuitive
class Grid(val width: Int = 5, cellsFrom: Array<Array<Cell>>? = null) {
    fun contains(point: Point): Boolean {
        return (point.col >= 0) and (point.col < width) and (point.row >=0) and (point.row < width)
    }

    fun getCellAt(position: Point): Cell {
        return cells[position.row][position.col]
    }

    fun buildAt(target: Point): Grid {
        val newCells = Array(width) { row ->
            Array(width) { col ->
                if ((row == target.row) and (col == target.col)) {
                    cells[row][col].build()
                } else {
                    cells[row][col]
                }
            }
        }

        return Grid(cellsFrom = newCells)
    }

    fun getSurroundingCells(position: Point): List<Cell> {
        val (row, col) = position
        val surrounding = mutableListOf<Cell>()
        for (direction in values()) {
            when (direction) {
                DOWN ->
                    if (row-1>=0) surrounding.add(cells[row-1][col])
                DOWN_LEFT ->
                    if ((row-1>=0) and (col-1>=0)) surrounding.add(cells[row-1][col-1])
                LEFT ->
                    if (col-1>=0) surrounding.add(cells[row][col-1])
                UP_LEFT ->
                    if ((row+1<width) and (col-1>=0)) surrounding.add(cells[row+1][col-1])
                UP ->
                    if (row+1<width) surrounding.add(cells[row+1][col])
                UP_RIGHT ->
                    if ((row+1<width) and (col+1<width)) surrounding.add(cells[row+1][col+1])
                RIGHT ->
                    if (col+1<width) surrounding.add(cells[row][col+1])
                DOWN_RIGHT ->
                    if ((row-1>=0) and (col+1<width))  surrounding.add(cells[row-1][col+1])
            }
        }
        return surrounding
    }

    val cells: Array<Array<Cell>>
    init {
        cells = cellsFrom ?: Array(width) { row ->
            Array(width) { col -> Cell(row,col) }
        }
    }
}

data class Builder(val god: God,
              val position: Point,
              val turnStage: TurnStage = TurnStage.MOVE,
              val isWinner: Boolean = false)  {

    fun canMove(state: GamePresenter.GameState): Boolean {
        return (!god.getAvailableMoves(state, position).isEmpty())
    }
    fun canBuild(state: GamePresenter.GameState): Boolean {
        return (!god.getAvailableBuilds(state, position).isEmpty())
    }
    fun move(state: GamePresenter.GameState, target: Point): Builder {
        return if (isValidMove(state, target, turnStage))
            Builder(god, target, TurnStage.BUILD, god.isWinner(state,target))
        else Builder(god, this.position, TurnStage.MOVE)
    }
    fun build(state: GamePresenter.GameState, target: Point): Builder {
        return if (isValidBuild(state, target, turnStage))
            Builder(god, this.position, TurnStage.MOVE)
        else Builder(god, this.position, TurnStage.BUILD)
    }

    fun getAvailableMoves(state: GamePresenter.GameState): List<Point> {
        return god.getAvailableMoves(state, position)
    }
    fun getAvailableBuilds(state: GamePresenter.GameState): List<Point> {
        return god.getAvailableBuilds(state, position)
    }

    private fun isValidMove(state: GamePresenter.GameState, target: Point, turnStage: TurnStage): Boolean {
        return (turnStage == TurnStage.MOVE) and (target in god.getAvailableMoves(state, position))
    }
    private fun isValidBuild(state: GamePresenter.GameState, target: Point, turnStage: TurnStage): Boolean {
        return (turnStage == TurnStage.BUILD) and (target in god.getAvailableBuilds(state, position))
    }

}
