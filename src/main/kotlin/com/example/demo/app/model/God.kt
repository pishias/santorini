package com.example.demo.app.model

import com.example.demo.app.presenter.GamePresenter
import kotlin.math.abs

interface God {
    fun getAvailableMoves(state: GamePresenter.GameState, position: Point): List<Point>
    fun getAvailableBuilds(state: GamePresenter.GameState, position: Point): List<Point>
    fun isWinner(state: GamePresenter.GameState, position: Point): Boolean
}

class SimpleGod : God {
    override fun getAvailableMoves(state: GamePresenter.GameState, position: Point): List<Point> {
        return state.grid.getSurroundingCells(position)
                .asSequence()
                .filter { it.canActHere() }
                .filter { abs(it.level - state.grid.getCellAt(position).level) <= 1 }
                .map { it.position }
                .filter { point -> listOf(state.builder1,state.builder2)
                        .all { it.position != point }
                }
                .sortedBy { it }
                .toList()
    }

    override fun getAvailableBuilds(state: GamePresenter.GameState, position: Point): List<Point> {
        return state.grid.getSurroundingCells(position)
                .filter { it.canActHere() }
                .map{ it.position }
                .sortedBy { it }

    }

    override fun isWinner(state: GamePresenter.GameState, position: Point): Boolean {
        return state.grid.cells[position.row][position.col].level == 3
    }

}