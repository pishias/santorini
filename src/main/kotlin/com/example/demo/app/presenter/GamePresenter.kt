package com.example.demo.app.presenter

import com.example.demo.app.elm.*
import com.example.demo.app.elm.presenter.IBasePresenter
import com.example.demo.app.model.*
import com.example.demo.app.model.GameStage.*
import com.example.demo.app.view.IGameView
import io.reactivex.Single
import io.reactivex.disposables.Disposable

class GamePresenter(private val view: IGameView,
                    private val program: Program<GameState>) : Component<GamePresenter.GameState>, IBasePresenter {

    data class GameState(val grid: Grid = Grid(),
                         val builder1: Builder,
                         val builder2: Builder,
                         val next: List<Point> = listOf(),
                         val gameStage: GameStage = PLAYER_1_MOVE) : State()

    class RequestMove(val builder: Builder, val target: Point): Msg()
    class RequestBuild(val builder: Builder, val target: Point): Msg()

    var programDisposable: Disposable

    init {
        programDisposable = program.init(initState(), this)
    }

    override fun update(msg: Msg, state: GameState): Pair<GameState, Cmd> {
        val state = state
        return when (msg) {

            is Init -> {
                Pair(initState(), None())
            }

            is RequestMove -> {
                // check which builder is requesting the move
                val currentBuilder = if (msg.builder == state.builder1) {
                    1
                } else {
                    2
                }

                // Check if lost
                if (!msg.builder.canMove(state)) return Pair(state.copy(gameStage = END), None())

                // check if move is in the acceptable set
                val newBuilder = msg.builder.move(state, msg.target)
                val gameStage: GameStage? = if (newBuilder.isWinner) END else null

                // pass the updated builder to the state
                return when (currentBuilder) {
                    1 -> {
                        val newState = state.copy(builder1 = newBuilder, gameStage = gameStage?: PLAYER_1_BUILD)
                        Pair(newState.copy(next = getAvailableBuilds(state, newState.builder1)), None())
                    }
                    2 -> {
                        val newState = state.copy(builder2 = newBuilder, gameStage = gameStage?: PLAYER_2_BUILD)
                        Pair(newState.copy(next = getAvailableBuilds(state, newState.builder2)), None())
                    }
                    else -> Pair(state, None())
                }
            }
            is RequestBuild -> {
                // check which builder is requesting the build
                val currentBuilder = if (msg.builder == state.builder1) {
                    1
                } else {
                    2
                }

                // Check if lost
                if (!msg.builder.canBuild(state)) return Pair(state.copy(gameStage = END), None())

                // check if build is in the acceptable one
                val newGrid: Grid
                val newBuilder = msg.builder.build(state, msg.target)
                if (newBuilder.turnStage == TurnStage.MOVE) {
                    newGrid = state.grid.buildAt(msg.target)
                    return when (currentBuilder) {
                        1 -> {
                            val newState = state.copy(grid = newGrid, builder1 = newBuilder, gameStage = PLAYER_2_MOVE)
                            Pair(newState.copy(next = getAvailableMoves(state, newState.builder2)), None())
                        }
                        2 -> {
                            val newState = state.copy(grid = newGrid, builder2 = newBuilder, gameStage = PLAYER_1_MOVE)
                            Pair(newState.copy(next = getAvailableMoves(state, newState.builder1)), None())
                        }
                        else -> Pair(state, None())
                    }
                }

                // pass the updated builder to the state
                return Pair(state, None())
            }
            else -> Pair(state, None())
        }
    }

    override fun render(state: GameState) {
        view.render(state)
    }

    override fun call(cmd: Cmd): Single<Msg> {
        return when (cmd) {
            else -> Single.just(Idle())
        }
    }

    fun init() {
        program.accept(Init())
    }

    fun move(builder: Builder, target: Point) {
        program.accept(RequestMove(builder, target))
    }

    fun build(builder: Builder, target: Point) {
        program.accept(RequestBuild(builder, target))
    }

    fun getCurrentPlayer(): Builder {
        val state = program.getState()
        if (state.gameStage.name.contains("PLAYER_2"))
            return state.builder2
        return state.builder1
    }

    fun getAvailableMoves(state: GameState, builder: Builder): List<Point> {
        return builder.getAvailableMoves(state)
    }

    fun getAvailableBuilds(state: GameState, builder: Builder): List<Point> {
        return builder.getAvailableBuilds(state)
    }

    fun getState(): GameState {
        return program.getState()
    }

    private fun initState(): GameState {
        val builder1 = Builder(SimpleGod(), Point(0, 0))
        val builder2 = Builder(SimpleGod(), Point(4, 4))
        val first = GameState(builder1 = builder1, builder2 = builder2)
        return GameState(builder1 = builder1, builder2 = builder2, next = getAvailableMoves(first, builder1))
    }
}