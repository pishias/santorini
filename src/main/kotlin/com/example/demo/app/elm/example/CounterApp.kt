package com.example.demo.app.elm.example

import com.example.demo.app.elm.Program
import io.reactivex.schedulers.Schedulers
import java.util.*

fun main(args: Array<String>) {
    val counterPresenter = CounterPresenter(CounterView(), Program(Schedulers.trampoline()))
    counterPresenter.init()
    val scan = Scanner(System.`in`)
    while (scan.next() != "0") counterPresenter.plusClick()
    counterPresenter.init()
}