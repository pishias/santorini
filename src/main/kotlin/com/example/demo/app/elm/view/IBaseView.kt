package com.example.demo.app.elm.view

import com.example.demo.app.elm.presenter.IBasePresenter

interface IBaseView {
    fun getPresenter(): IBasePresenter
}