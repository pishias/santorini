package com.example.demo.app.elm.example

import com.example.demo.app.elm.*
import io.reactivex.Single
import io.reactivex.disposables.Disposable

class CounterPresenter(private val view: CounterView,
                       private val program: Program<CounterState>) : Component<CounterPresenter.CounterState> {

    data class CounterState(val value: Int = 0) : State()

    class Inc : Msg()
    class Dec : Msg()

    var programDisposable: Disposable

    init {
        programDisposable = program.init(CounterState(), this)
    }

    fun init() {
        program.accept(Init())
    }


    override fun update(msg: Msg, state: CounterState): Pair<CounterState, Cmd> {
        return when (msg) {
            is Init -> {
                Pair(CounterState(), None())
            }
            is Inc -> {
                Pair(state.copy(value = state.value+1), None())
            }
            is Dec -> {
                Pair(state.copy(value = state.value-1), None())
            }
            else -> Pair(state, None())
        }
    }

    override fun render(state: CounterState) {
        view.showValue(state.value)
    }

    override fun call(cmd: Cmd): Single<Msg> {
        return when (cmd) {
            else -> Single.just(Idle())
        }
    }

    fun plusClick() {
        program.accept(Inc())
    }

    fun minusClick() {
        program.accept(Dec())
    }

    fun onDestroy(){
        if (!programDisposable.isDisposed){
            programDisposable.dispose()
        }
    }

}