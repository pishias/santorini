package com.example.demo.app.ignore

import com.example.demo.app.ignore.views.MainView
import tornadofx.*


class MyApp: App(MainView::class, Styles::class)