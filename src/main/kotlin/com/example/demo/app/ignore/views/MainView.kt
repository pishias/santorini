package com.example.demo.app.ignore.views

import com.example.demo.app.model.*
import com.example.demo.app.presenter.GamePresenter
import javafx.concurrent.Task
import javafx.geometry.Point2D
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import tornadofx.*

class MainView : View("Hello TornadoFX") {
    private val PADDING = 10.0
    private val RECTANGLE_HEIGHT = 100.0
    private val RECTANGLE_WIDTH = 100.0

    private val BUILDER_HEIGHT = 20.0
    private val BUILDER_WIDTH = 20.0

    private var myGrid: Grid = Grid()

    val builder1p1 = Builder(SimpleGod(), Point(0,0))
    //val builder2p1 = Builder('B', SimpleGod(), Point(0,3))
    val builder1p2 = Builder(SimpleGod(), Point(2,0))
    //val builder2p2 = Builder('Y', SimpleGod(), Point(2,3))

    private val objects = mutableListOf<Node>()
    private var board: Parent by singleAssign()

    private val initState = GamePresenter.GameState(
            grid = myGrid,
            builder1 = builder1p1,
            builder2 = builder1p2
    )

    private val colors = listOf(Color.GRAY, Color.YELLOW, Color.BLUE, Color.GREEN, Color.RED)
    private val playerColor = listOf(Color.WHITE, Color.DARKCYAN, Color.DARKMAGENTA)

    override val root =  stackpane {
        board = group {
            fun createCell(cell: Cell): Rectangle {
                return rectangle {
                    width=RECTANGLE_WIDTH
                    height=RECTANGLE_HEIGHT
                    fill = colors[cell.level]
                    properties["rectColor"] = colors[cell.level]
                    properties["type"] = "cell"
                    x = (RECTANGLE_WIDTH+PADDING) * cell.position.col
                    y = (RECTANGLE_HEIGHT+PADDING) * cell.position.row
                    // addClass(DraggingStyles.toolboxItem)
                }
            }

            fun createPlayer(builder: Builder): Rectangle {
                //for (builder in builder.builders) {
                val color = if (builder == builder1p1) playerColor[1] else playerColor[2]
                return rectangle {
                    width = BUILDER_WIDTH
                    height = BUILDER_HEIGHT
                    fill = color
                    properties["rectColor"] = color
                    properties["type"] = "builder"
                    x = (RECTANGLE_WIDTH+PADDING) * builder.position.col  + (RECTANGLE_WIDTH-BUILDER_WIDTH) / 2
                    y = (RECTANGLE_HEIGHT+PADDING) * builder.position.row + (RECTANGLE_HEIGHT-BUILDER_HEIGHT) / 2
                }
                //}
            }

            hboxConstraints {
                paddingAll = 50
                alignment = Pos.BOTTOM_LEFT
                hgrow = Priority.ALWAYS
            }

            //shapes will go here
            for (col in 0 until myGrid.width) {
                for (row in 0 until myGrid.width) {
                    add(createCell(myGrid.cells[row][col]))
                }
            }

            val player1 = createPlayer(builder1p1)
            val player2 = createPlayer(builder1p2)
            add(player1)
            add(player2)

            player1.setOnDragDetected { event ->
                val task = object : Task<Void>() {
                    @Throws(Exception::class)
                    override fun call(): Void? {
                        for (i in 1..10) {
                            updateMessage("Count: $i")
                            Thread.sleep(250)
                        }
                        return null
                    }
                }
                task.messageProperty().addListener { obs, oldMessage, newMessage -> print(newMessage) }
                Thread(task).start()
            }

//            addEventFilter(MouseEvent.MOUSE_PRESSED, ::startDrag)
//            addEventFilter(MouseEvent.MOUSE_DRAGGED, ::animateDrag)
//            addEventFilter(MouseEvent.MOUSE_EXITED, ::stopDrag)
//            addEventFilter(MouseEvent.MOUSE_RELEASED, ::stopDrag)
//            addEventFilter(MouseEvent.MOUSE_RELEASED, ::drop)
        }
    }

    init {
        objects.addAll( board.childrenUnmodifiable )
    }

    private fun startDrag(evt : MouseEvent) {

        objects
                .filter {
                    it.properties["type"] == "builder"
                }.firstOrNull {
                    val mousePt: Point2D = it.sceneToLocal(evt.sceneX, evt.sceneY)
                    it.contains(mousePt)
                }
                .apply {
                    if( this != null ) {
                        println(this)
                    }
                }
    }

//    private fun animateDrag(evt : MouseEvent) {
//        objects
//                .filter {
//                    it.properties["type"] == "builder"
//                }
//                .filter {
//                    val mousePt : Point2D = it.sceneToLocal( evt.sceneX, evt.sceneY )
//                    it.contains(mousePt)
//                }
//                .firstOrNull()
//                .apply {
//                    if( this != null ) {
//                        println(this)
//                    }
//                }
//    }
}